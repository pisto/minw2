#include <cmath>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <string>
#include <algorithm>
#include <boost/program_options.hpp>
#include <armadillo>

using namespace std;

namespace {

int print_fatal_error(const string& msg) {
	cerr << msg << endl;
	return 1;
}

struct setup {
	uint16_t N;
	arma::vec omegas;
	arma::mat eigenvectors;
	double delta, cutoff;
	bool overlap_only;
};

#ifdef NDEBUG
[[gnu::target_clones("default,avx")]]
#endif
double minW2_4(const setup& s, uint16_t i) {
	struct [[gnu::packed]] tuple_ij {
		uint16_t j;
		double omegas_sum, omegas_times;
		tuple_ij(const setup& s, uint16_t i, uint16_t j):
				j(j), omegas_sum(s.omegas[i] + s.omegas[j]), omegas_times(s.omegas[i] * s.omegas[j]) {}
	};
	/*
	 * Gather all the sides of a 2-2 interaction with exactly one "wave number" being the selected one (i) in tuples_ij.
	 * Precompute the overlap multiplication of the i-th eigenvectors with the other eigenvectors in vectors_ij.
	 */
	vector<tuple_ij> tuples_ij;
	arma::mat vectors_ij = s.eigenvectors;
	double vector_cutoff = pow(s.cutoff, 0.25);
	for (uint16_t j = 0; j < s.N; j++) {
		vectors_ij.col(j) %= s.eigenvectors.col(i);
		if (j != i && max(abs(vectors_ij.col(j))) >= vector_cutoff)
			tuples_ij.emplace_back(s, i, j);   //this will be sorted by increasing omegas_sum
	}
	//precalculate the right hand-side of an interaction of the type (j, k, i, i)
	tuple_ij tuple_ii(s, i, i);

	double W2 = 0;
	/*
	 * 2-2 interactions with no i-th wave numbers on the left and one to the right.
	 * A resonance with a tuple_ij will have a delta_omega = omega_k + omega_l - omega_i - omega_j: since tuples_ij
	 * are sorted by (omega_i - omega_j), for a given (k, l) pair we can find the first resonance such that
	 * |delta_omega| < delta, then loop on the sorted resonances until delta_omega >= -delta, stop there, pick another
	 * (k, l) pair and reiterate.
	 */
	arma::vec vector_jk(s.N);     //preallocate overlap vector
	for (uint16_t j = 0; j < s.N; j++) {
		if (j == i) continue;
		uint16_t il_start_hint = 0;
		for (uint16_t k = j; k < s.N; k++) {
			if (k == i) continue;
			vector_jk = s.eigenvectors.col(j) % s.eigenvectors.col(k);
			if (vector_cutoff > 0 && max(abs(vector_jk)) < vector_cutoff) continue;
			double omegajk_sum = s.omegas[j] + s.omegas[k], omegajk_times = s.overlap_only ? 0 : omegajk_times = s.omegas[j] * s.omegas[k];
			//2-2 interactions with no i-th wave numbers on the left and 2 to the right
			if (abs(omegajk_sum - tuple_ii.omegas_sum) <= s.delta) {
				double overlap = arma::dot(vector_jk, vectors_ij.col(i));
				overlap *= overlap;
				if (!s.overlap_only) overlap /= omegajk_times * tuple_ii.omegas_times;
				W2 += overlap;
			}
			//2-2 interactions with no i-th wave numbers on the left and 1 to the right
			while (il_start_hint < tuples_ij.size() && omegajk_sum - tuples_ij[il_start_hint].omegas_sum > s.delta)
				il_start_hint++;
			if (il_start_hint == tuples_ij.size()) break;
			for (uint16_t il = il_start_hint; il < tuples_ij.size(); il++) {
				auto& tuple_il = tuples_ij[il];
				if (abs(omegajk_sum - tuple_il.omegas_sum) > s.delta) break;
				double overlap = arma::dot(vector_jk, vectors_ij.col(tuple_il.j));
				overlap *= overlap;
				if (!s.overlap_only) overlap /= omegajk_times * tuple_il.omegas_times;
				W2 += overlap;
			}
		}
	}
	//2-2 interactions with one i-th wave numbers on the left and one to the right
	for (uint16_t ij = 0; ij < tuples_ij.size(); ij++) {
		auto& tuple_ij = tuples_ij[ij];
		auto vector_ij = vectors_ij.col(tuple_ij.j);
		/*
		 * Start from j + 1 so interactions are not double counted. The (i, k) tuple will have an ascending
		 * (omega_i + omega_k) term just like before, so stop when delta_omega < -delta.
		 */
		for (uint16_t ik = ij + 1; ik < tuples_ij.size(); ik++) {
			auto& tuple_ik = tuples_ij[ik];
			if (tuple_ij.omegas_sum - tuple_ik.omegas_sum < -s.delta) break;
			double overlap = arma::dot(vector_ij, vectors_ij.col(tuple_ik.j));
			overlap *= overlap;
			if (!s.overlap_only) overlap /= tuple_ij.omegas_times * tuple_ik.omegas_times;
			W2 += overlap;
		}
		//2-2 interactions with 1 i-th wave numbers on the left and 2 to the right
		if (abs(tuple_ij.omegas_sum - tuple_ii.omegas_sum) > s.delta) continue;
		double overlap = arma::dot(vector_ij, vectors_ij.col(i));
		overlap *= overlap;
		if (!s.overlap_only) overlap /= tuple_ij.omegas_times * tuple_ii.omegas_times;
		W2 += overlap;
	}

	return W2;
}

#ifdef NDEBUG
[[gnu::target_clones("default,avx")]]
#endif
double minW2_6(const setup& s, uint16_t i) {
	struct [[gnu::packed]] tuple_ijk {
		uint16_t j, k;
		double omegas_sum;
		tuple_ijk(const setup& s, uint16_t i, uint16_t j, uint16_t k): j(min(j, k)), k(max(j, k)), omegas_sum(s.omegas[i] + s.omegas[j] + s.omegas[k]) {}
	};
	double vector_cutoff = pow(s.cutoff, 1 / 6.);
	vector<tuple_ijk> tuples_ijk;
	arma::cube vectors_jk(s.N, s.N, s.N);
	for (uint16_t j = 0; j < s.N; j++) {
		auto& vectors_j = vectors_jk.slice(j);
		for (uint16_t k = j == i ? 0 : j; k < s.N; k++) {
			auto vector_jk = vectors_j.col(k);
			vector_jk = s.eigenvectors.col(j) % s.eigenvectors.col(k);
			if (j != i && k != i && (vector_cutoff == 0 || max(abs(s.eigenvectors.col(i) % vector_jk)) >= vector_cutoff))
				tuples_ijk.emplace_back(s, i, j, k);
		}
	}
	sort(tuples_ijk.begin(), tuples_ijk.end(), [](const tuple_ijk& a, const tuple_ijk& b){ return a.omegas_sum < b.omegas_sum; });
	struct [[gnu::packed]] tuple_iij {
		uint16_t j;
		double omegas_sum;
		tuple_iij(const setup& s, uint16_t i, uint16_t j): j(j), omegas_sum(2 * s.omegas[i] + s.omegas[j]) {}
	};
	vector<tuple_iij> tuples_iij;
	arma::mat& vectors_iij = vectors_jk.slice(i);
	for (uint16_t j = 0; j < s.N; j++) {
		vectors_iij.col(j) %= s.eigenvectors.col(i);
		if (j != i && max(abs(vectors_iij.col(j))) >= vector_cutoff)
			tuples_iij.emplace_back(s, i, j);
	}
	//precalculate the right hand-side of an interaction of the type (j, k, l, i, i, i)
	tuple_iij tuple_iii(s, i, i);

	double W2 = 0;
	arma::vec vector_jkl(s.N);
	for (uint16_t j = 0; j < s.N; j++) {
		if (j == i) continue;
		for (uint16_t k = j; k < s.N; k++) {
			if (k == i) continue;
			auto vector_jk = vectors_jk.slice(j).col(k);
			double omegajk_sum = s.omegas[j] + s.omegas[k];
			uint16_t iim_start_hint = 0, imn_start_hint = 0;
			for (uint16_t l = k; l < s.N; l++) {
				if (l == i) continue;
				vector_jkl = vector_jk % s.eigenvectors.col(l);
				if (vector_cutoff > 0 && max(abs(vector_jkl)) < vector_cutoff) continue;
				double omegajkl_sum = omegajk_sum + s.omegas[l];
				//0 - 3
				if (abs(omegajkl_sum - tuple_iii.omegas_sum) <= s.delta) {
					double overlap = arma::dot(vector_jkl, vectors_iij.col(i));
					W2 += overlap * overlap;
				}
				//0 - 2
				while (iim_start_hint < tuples_iij.size() && omegajkl_sum - tuples_iij[iim_start_hint].omegas_sum > s.delta)
					iim_start_hint++;
				for (uint16_t iim = iim_start_hint; iim < tuples_iij.size(); iim++) {
					auto& tuple_iim = tuples_iij[iim];
					if (abs(omegajkl_sum - tuple_iim.omegas_sum) > s.delta) break;
					double overlap = arma::dot(vector_jkl, vectors_iij.col(tuple_iim.j));
					W2 += overlap * overlap;
				}
				//0 - 1
				while (imn_start_hint < tuples_ijk.size() && omegajkl_sum - tuples_ijk[imn_start_hint].omegas_sum > s.delta)
					imn_start_hint++;
				for (uint16_t imn = imn_start_hint; imn < tuples_ijk.size(); imn++) {
					auto& tuple_imn = tuples_ijk[imn];
					if (abs(omegajkl_sum - tuple_imn.omegas_sum) > s.delta) break;
					double overlap = arma::dot(vector_jkl, s.eigenvectors.col(i) % vectors_jk.slice(tuple_imn.j).col(tuple_imn.k));
					W2 += overlap * overlap;
				}
				if (iim_start_hint >= tuples_iij.size() && imn_start_hint >= tuples_ijk.size()) break;
			}
		}
	}

	arma::vec& vector_ijk = vector_jkl;
	uint16_t iil_start_hint = 0;
	for (uint16_t ijk = 0; ijk < tuples_ijk.size(); ijk++) {
		auto& tuple_ijk = tuples_ijk[ijk];
		vector_ijk = s.eigenvectors.col(i) % vectors_jk.slice(tuple_ijk.j).col(tuple_ijk.k);
		//1 - 3
		if (abs(tuple_ijk.omegas_sum - tuple_iii.omegas_sum) <= s.delta) {
			double overlap = arma::dot(vector_ijk, vectors_iij.col(i));
			W2 += overlap * overlap;
		}
		//1 - 2
		while (iil_start_hint < tuples_iij.size() && tuple_ijk.omegas_sum - tuples_iij[iil_start_hint].omegas_sum > s.delta)
			iil_start_hint++;
		for (uint16_t iil = iil_start_hint; iil < tuples_iij.size(); iil++) {
			auto& tuple_iil = tuples_iij[iil];
			if (abs(tuple_ijk.omegas_sum - tuple_iil.omegas_sum) > s.delta) break;
			double overlap = arma::dot(vector_ijk, vectors_iij.col(tuple_iil.j));
			W2 += overlap * overlap;
		}
		//1 - 1
		for (uint16_t ilm = ijk + 1; ilm < tuples_ijk.size(); ilm++) {
			auto& tuple_ilm = tuples_ijk[ilm];
			if (tuple_ijk.omegas_sum - tuple_ilm.omegas_sum < -s.delta) break;
			double overlap = arma::dot(vector_ijk, s.eigenvectors.col(i) % vectors_jk.slice(tuple_ilm.j).col(tuple_ilm.k));
			W2 += overlap * overlap;
		}
	}

	for (uint16_t iij = 0; iij < tuples_iij.size(); iij++) {
		auto& tuple_iij = tuples_iij[iij];
		auto vector_iij = vectors_iij.col(tuple_iij.j);
		//2 - 3
		if (abs(tuple_iij.omegas_sum - tuple_iii.omegas_sum) <= s.delta) {
			double overlap = arma::dot(vector_iij, vectors_iij.col(i));
			W2 += overlap * overlap;
		}
		//2 - 2
		for (uint16_t iik = iij + 1; iik < tuples_iij.size(); iik++) {
			auto& tuple_iik = tuples_iij[iik];
			if (tuple_iij.omegas_sum - tuple_iik.omegas_sum < -s.delta) break;
			double overlap = arma::dot(vector_iij, vectors_iij.col(tuple_iik.j));
			W2 += overlap * overlap;
		}
	}

	return W2;
}

}

int main(int argc, char** argv) try {
	setup s;
	bool six;
	{
		using namespace boost::program_options;
		options_description options("Options for minW2");
		options.add_options()
				(",d", value(&s.delta)->required(), "delta omega")
				("cutoff", value(&s.cutoff)->default_value(0), "cutoff value for squared overlap integral")
				("six,6", "calculate 6-wave interactions")
				("overlap", "calculate only overlap term, do not divide by frequencies");
		variables_map vm;
		store(parse_command_line(argc, argv, options), vm);
		notify(vm);

		six = vm.count("six"), s.overlap_only = vm.count("overlap") | six;
		s.delta = abs(s.delta), s.cutoff = abs(s.cutoff);
		vector<double> ms;
		double m;
		while (cin.read((char*)&m, sizeof(double))) ms.push_back(m);
		if (ms.size() < 2 || ms.size() >= 0xFFFF) throw invalid_argument("number of masses out of range");
		cerr << "N=" << (s.N = ms.size()) << endl;
		arma::mat interaction = diagmat(arma::vec(ms) + 2);
		interaction.diag(1).fill(-1);
		interaction.diag(-1).fill(-1);
		interaction(0, s.N - 1) = interaction(s.N - 1, 0) = -1;
		if (!arma::eig_sym(s.omegas, s.eigenvectors, interaction))
			throw std::runtime_error("Cannot calculate eigensystem!");
		s.omegas = sqrt(s.omegas);
	}

	vector<double> minW2s(s.N);
	#pragma omp parallel for
	for (uint16_t i = 0; i < s.N; i++) {
		cerr << '.';
		minW2s[i] = six ? minW2_6(s, i) : minW2_4(s, i);
	}
	cerr << endl;

	cout.write((char*)minW2s.data(), s.N * sizeof(double));

} catch (const ios_base::failure& e) {
	return print_fatal_error("I/O error, "s + e.what() + " (" + e.code().message() + ")");
} catch (const invalid_argument& e) {
	return print_fatal_error("invalid argument, "s + e.what());
} catch (const boost::program_options::error& e) {
	return print_fatal_error("invalid argument, "s + e.what());
}
